/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.abstractanimal;

/**
 *
 * @author Nippon
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        h1.sleep();
        h1.speak();
        System.out.println("h1 is Animal ? : " + (h1 instanceof Animal));
        System.out.println("h1 is LandAnimal ? : " + (h1 instanceof LandAnimal));
        Animal a1 = h1;
        System.out.println("a1 is Reptile ? : " + (a1 instanceof Reptile));
        System.out.println("a1 is Aqualtic ? : " + (a1 instanceof Aqualtic));
        System.out.println("a1 is Poultry ? : " + (a1 instanceof Poultry));
        System.out.println("----------BYE BYE----------");

        Dog d1 = new Dog("Mumu");
        d1.eat();
        d1.walk();
        d1.run();
        d1.sleep();
        d1.speak();
        System.out.println("d1 is Animal ? : " + (d1 instanceof Animal));
        System.out.println("d1 is LandAnimal ? : " + (d1 instanceof LandAnimal));
        Animal a2 = d1;
        System.out.println("a2 is Reptile ? : " + (a2 instanceof Reptile));
        System.out.println("a2 is Aqualtic ? : " + (a2 instanceof Aqualtic));
        System.out.println("a2 is Poultry ? : " + (a2 instanceof Poultry));
        System.out.println("----------BYE BYE----------");
        Cat c1 = new Cat("Somsri");
        c1.eat();
        c1.walk();
        c1.run();
        c1.sleep();
        c1.speak();
        System.out.println("c1 is Animal ? : " + (c1 instanceof Animal));
        System.out.println("c1 is LandAnimal ? : " + (c1 instanceof LandAnimal));
        Animal a3 = c1;
        System.out.println("a3 is Reptile ? : " + (a3 instanceof Reptile));
        System.out.println("a3 is Aqualtic ? : " + (a3 instanceof Aqualtic));
        System.out.println("a3 is Poultry ? : " + (a3 instanceof Poultry));
        System.out.println("----------BYE BYE----------");
        System.out.println("********************");
        System.out.println("*                  *");
        System.out.println("*                  *");
        System.out.println("********************");

        Bird b1 = new Bird("Tweetzer");
        b1.eat();
        b1.walk();
        b1.fly();
        b1.sleep();
        b1.speak();
        System.out.println("b1 is Animal ? : " + (b1 instanceof Animal));
        System.out.println("b1 is Poultry ? : " + (b1 instanceof Poultry));
        Animal x1 = b1;
        System.out.println("x1 is Reptile ? : " + (x1 instanceof Reptile));
        System.out.println("x1 is Aqualtic ? : " + (x1 instanceof Aqualtic));
        System.out.println("x1 is LandAnimal ? : " + (x1 instanceof LandAnimal));
        System.out.println("----------BYE BYE----------");
        Bat b2 = new Bat("BatBat");
        b2.eat();
        b2.walk();
        b2.fly();
        b2.sleep();
        b2.speak();
        System.out.println("b2 is Animal ? : " + (b2 instanceof Animal));
        System.out.println("b2 is Poultry ? : " + (b2 instanceof Poultry));
        Animal x2 = b2;
        System.out.println("x2 is Reptile ? : " + (x2 instanceof Reptile));
        System.out.println("x2 is Aqualtic ? : " + (x2 instanceof Aqualtic));
        System.out.println("x2 is LandAnimal ? : " + (x2 instanceof LandAnimal));
        System.out.println("----------BYE BYE----------");
        System.out.println("********************");
        System.out.println("*                  *");
        System.out.println("*                  *");
        System.out.println("********************");

        Snake s1 = new Snake("Wong Fei Hung");
        s1.crawl();
        s1.eat();
        s1.sleep();
        s1.speak();
        s1.walk();
        System.out.println("s1 is Animal ? : " + (s1 instanceof Animal));
        System.out.println("s1 is Reptile ? : " + (s1 instanceof Reptile));
        Animal y1 = s1;
        System.out.println("y1  is Poultry ? : " + (y1 instanceof Poultry));
        System.out.println("y1  is Aqualtic ? : " + (y1 instanceof Aqualtic));
        System.out.println("y1  is LandAnimal ? : " + (y1 instanceof LandAnimal));
        System.out.println("----------BYE BYE----------");
        Crocodile c2 = new Crocodile("Here");
        c2.crawl();
        c2.eat();
        c2.sleep();
        c2.speak();
        c2.walk();
        System.out.println("c2 is Animal ? : " + (c2 instanceof Animal));
        System.out.println("c2 is Reptile ? : " + (c2 instanceof Reptile));
        Animal y2 = c2;
        System.out.println("y2 is Poultry ? : " + (y1 instanceof Poultry));
        System.out.println("y2 is Aqualtic ? : " + (y1 instanceof Aqualtic));
        System.out.println("y2 is LandAnimal ? : " + (y1 instanceof LandAnimal));
        System.out.println("----------BYE BYE----------");
        System.out.println("********************");
        System.out.println("*                  *");
        System.out.println("*                  *");
        System.out.println("********************");

        Fish f1 = new Fish("Thong");
        f1.eat();
        f1.sleep();
        f1.speak();
        f1.walk();
        f1.swim();
        System.out.println("f1 is Animal ? : " + (f1 instanceof Animal));
        System.out.println("f1 is Aqualtic ? : " + (f1 instanceof Aqualtic));
        Animal z1 = f1;
        System.out.println("z1 is Poultry ? : " + (z1 instanceof Poultry));
        System.out.println("z1 is Reptile ? : " + (z1 instanceof Reptile));
        System.out.println("z1 is LandAnimal ? : " + (z1 instanceof LandAnimal));
        System.out.println("----------BYE BYE----------");
        Crab c3 = new Crab ("ma");
        c3.eat();
        c3.walk();
        c3.swim();
        c3.sleep();
        c3.speak();
        System.out.println("c3 is Animal ? : " + (c3 instanceof Animal));
        System.out.println("c3 is Aqualtic ? : " + (c3 instanceof Aqualtic));
        Animal z2 = c3;
        System.out.println("z2 is Poultry ? : " + (z2 instanceof Poultry));
        System.out.println("z2 is Reptile ? : " + (z2 instanceof Reptile));
        System.out.println("z2 is LandAnimal ? : " + (z2 instanceof LandAnimal));
        System.out.println("----------BYE BYE----------");
        System.out.println("----------END PROJECT----------");
        
    }
}
