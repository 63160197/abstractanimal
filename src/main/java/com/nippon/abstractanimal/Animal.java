/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.abstractanimal;

/**
 *
 * @author Nippon
 */
public abstract class Animal {
    private String name;
    private int numberofleg;
    
    public Animal(String name , int numberofleg){
        this.name=name;
        this.numberofleg=numberofleg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberofleg() {
        return numberofleg;
    }

    public void setNumberofleg(int numberofleg) {
        this.numberofleg = numberofleg;
    }

    @Override
    public String toString() {
        return "Animal{" + "name=" + name + ", numberofleg=" + numberofleg + '}';
    }
    public abstract void eat();
    public abstract void walk();
    public abstract void speak();
    public abstract void sleep();
    
}
