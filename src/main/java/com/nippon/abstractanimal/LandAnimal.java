/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.abstractanimal;

/**
 *
 * @author Nippon
 */
public abstract class LandAnimal extends Animal {

    public LandAnimal(String name, int numberofleg) {
        super(name, numberofleg);
    }

    public abstract void run();

}
