/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.abstractanimal;

/**
 *
 * @author Nippon
 */
public class Bat extends Poultry {

    private String name;

    public Bat(String name) {
        super("Bat", 2);
        this.name=name;
    }

    @Override
    public void fly() {
        System.out.println("Bat : " + name + " fly");
    }

    @Override
    public void eat() {
         System.out.println("Bat : " + name + " eat");
    }

    @Override
    public void walk() {
         System.out.println("Bat : " + name + " walk");
    }

    @Override
    public void speak() {
         System.out.println("Bat : " + name + " speak");
    }

    @Override
    public void sleep() {
         System.out.println("Bat : " + name + " sleep");
    }

}
